alias please='sudo'

# Most important part of a UNIX system
alias btw='fastfetch'
alias neofetch='fastfetch'

# Edit .zshrc and reload it on exit
alias zshrc='vim ~/.zshrc ; source ~/.zshrc'
alias zshr='source ~/.zshrc'

# Make the commands more verbose and human readable
alias ll='ls -1 --color=auto'
alias la='ls -lAh --color=auto'
alias grep='grep --color=auto'
alias df='df -h'
alias free='free -m'

# Use nvim instead of vi(m)
alias vi='nvim'
alias vim='nvim'

# Utility functions
mkcd() {
    mkdir -p $1 && cd $1
}
cds() {
    cd $1 && la
}
cls() {
    clear && la
}

# Git stuff
alias gada='git add .'
alias gadu='git add -u'
alias gbr='git branch'
alias gch='git checkout'
alias gcom='git commit -m'
alias gf='git fetch'
alias gdiff="git diff"
alias gpl='git pull origin'
alias gps='git push origin'
alias gs='git status'
alias gt='git tag'
alias gtn='git tag -a'
