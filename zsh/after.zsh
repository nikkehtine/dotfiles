## Variables and paths that need to be loaded last

if command -v pacman > /dev/null; then
    source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2> /dev/null
    source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh 2> /dev/null
elif [[ $(uname) == "Linux" ]]; then
    source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2> /dev/null
    source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh 2> /dev/null
elif [[ $(uname) == "Darwin" ]]; then
    source $(brew --prefix)/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2> /dev/null
    source $(brew --prefix)/share/zsh-autosuggestions/zsh-autosuggestions.zsh 2> /dev/null
fi

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"
