####    GLOBAL

# GPG env variable
export GPG_TTY=$(tty)

# Add .scripts to path
export PATH=$PATH:~/.scripts

####    SOFTWARE

# C3
export PATH=$PATH:$HOME/.c3
# Rust
. "$HOME/.cargo/env"
# Go
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$HOME/go/bin
# VSCode WSL
export PATH=$PATH:"/mnt/c/Users/nwaru/AppData/Local/Programs/Microsoft VS Code/bin"
# pnpm
export PNPM_HOME="/home/nikkeh/.local/share/pnpm"
case ":$PATH:" in
*":$PNPM_HOME:"*) ;;
*) export PATH="$PNPM_HOME:$PATH" ;;
esac
# bun completions
[ -s "/home/nikkeh/.bun/_bun" ] && source "/home/nikkeh/.bun/_bun"
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"
# Nix
if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
  . '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi
# End Nix
export PATH=$PATH:/nix/var/nix/profiles/default/bin:$HOME/.nix-profile/bin

####    OS SPECIFIC

# macOS
if [[ $(uname) == "Darwin" ]]; then
  fpath+=("$(brew --prefix)/share/zsh/site-functions")
  export LIBRARY_PATH="$LIBRARY_PATH:/opt/homebrew/lib"
fi
